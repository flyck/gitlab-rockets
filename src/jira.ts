export function getJiraKey(s: string, default_return = "?"): string {
  var jira_matcher = /\d+-[A-Z]+(?!-?[a-zA-Z]{1,10})/g

  s = s.split("").reverse().join("")
  var m = s.match(jira_matcher);

  // Also need to reverse all the results!
  if (m) {
    return m[0].split("").reverse().join("")
  }
  return default_return
}
