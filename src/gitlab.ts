import { updateStatus } from "./animate/text";

export async function getAllDeployments(projects: string[], apiKey: string): Promise<job[]> {
  var promises: any[] = []
  var currentDeployments: any[] = []
  const oneHour = 60 * 60
  const days = 3
  const duration = oneHour * 24 * days

  updateStatus(`Loading deployments for last ${days} days`, true)

  projects.forEach(project => {
    promises.push(
      new Promise(
        async (resolve, _) => {
          const newDeployments = getDeploymentsFromJobs(await getCurrentJobs(project, duration, apiKey))

          if (newDeployments.length > 0) {
            currentDeployments = currentDeployments.concat(newDeployments)
            console.log(`found new deployments for project ${project}: ${JSON.stringify(newDeployments)}`)
          }
          resolve("")
        }
      )
    )

  });

  await Promise.all(promises)
  updateStatus(`Deployments loaded! Found ${currentDeployments.length}`, false, true)
  return currentDeployments
}

async function getCurrentJobs(projectId: string, deltaSeconds: number = 60, apiKey: string, page: number = 1): Promise<job[]> {
  var jobs: any = []

  // docs: https://docs.gitlab.com/ee/api/jobs.html
  await getData(`https://gitlab.com/api/v4/projects/${projectId}/jobs?page=${page}`, apiKey).then(
    async data => {
      if (!Array.isArray(data)) return []

      data.forEach((job: any) => {
        // skip jobs that havent been started
        if (!["manual", "skipped"].includes(job["status"])) {
          var deltaDate = new Date(Date.now() - 1000 * deltaSeconds);
          var jobDate = new Date(job["created_at"])

          if (jobDate > deltaDate) {
            jobs.push(
              {
                created_at: job["created_at"],
                author: job["user"]["name"],
                seconds: jobDate.getSeconds(),
                ref: job["ref"],
                stage: job["stage"],
                name: job["name"],
                user_pic_url: job["user"]["avatar_url"],
                web_url: job["web_url"],
                status: job["status"],
                repo_url: job["web_url"].split("/-/")[0],
                repo_name: job["web_url"].split("gitlab.com/")[1].split("/-/")[0]
              } as job
            )
          }
        }
      });

      // if any deployments are within our delta range, also check out the next page!
      if (jobs.length > 0 && page < 5) {
        page += 1
        jobs = jobs.concat(await getCurrentJobs(projectId, deltaSeconds, apiKey, page))
      }
    }
  )

  return jobs;
}

// TODO check if we can use the api filter for this instead
// on the other hand, scanning just jobs from the last minute wont be too costly
function getDeploymentsFromJobs(jobs: any) {
  var result: any[] = []

  // TODO problem: stage name != environment.. environment would be better
  // on the other hand, in practice this will be close to perfect
  jobs.forEach((job: job) => {
    if (job["name"].includes("deploy") || job["stage"].includes("deploy")) {
      result.push(job)
    }
  });

  return result
}

export async function loadProjects(): Promise<string[]> {
  const config = require('./../config.json')

  updateStatus("Loading projects", true)

  let projects
  let configLoaded = false
  try {
    projects = JSON.parse(sessionStorage.getItem('projects') as string);
    if (projects) configLoaded = true
  } catch (e) {
    updateStatus("Error loading projects", true)
    throw ("[Loader] Error loading config: " + e)
  }

  if (!configLoaded) {
    try {
      projects = await getProjects(config.GITLAB_TOKEN)

      sessionStorage.setItem('projects', JSON.stringify(projects));
    } catch (e) {
      updateStatus("Error retrieving projects", true)
      throw ("[Loader] Error retrieving projects: " + e)
    }
  }

  updateStatus("Projects loaded!", false, true)
  console.log(JSON.stringify(projects))

  return projects
}

async function getProjects(apiKey: string, page: number = 1) {
  var projects: any = []

  await getData(`https://gitlab.com/api/v4/projects?membership=true&page=${page}`, apiKey).then(
    async data => {
      if (!Array.isArray(data)) return []
      data.forEach(element => {
        projects.push(element.id)
      });

      if (data.length > 0) {
        page += 1
        projects = projects.concat(await getProjects(apiKey, page))
      }
    }
  )

  return projects
}

export async function getData(url: string, apiKey: string) {
  const response = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'PRIVATE-TOKEN': apiKey
    }
  })

  return response.json();
}
