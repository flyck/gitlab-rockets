export function updateStatus(text: string, dot_animation = false, fade_animation = false) {
  console.log(`[UpdateStatus]: dot_aniomation ${dot_animation}, fade ${fade_animation}: ${text}`)

  var status_text = document.getElementById("status_text");
  if (status_text) {
    status_text.innerText = text

    // do an animation if we get the dots
    var step = 1
    if (dot_animation) draw_dots()
    function draw_dots() {
      if (status_text) {
        // abort if the text was already udpated with a new status
        if (status_text.innerHTML.indexOf(text) != 0) return

        status_text.innerText = text + ".".repeat(step)
        step = step + 1

        setTimeout(draw_dots, 1000)
      }
    }

    // fade away animation
    if (fade_animation) fade()
    function fade() {
      if (status_text) {
        var opacity = parseFloat(status_text.style.opacity)
        if (opacity > 0) {
          status_text.style.opacity = (opacity - 0.003).toString()
          setTimeout(fade, 5)
        } else {
          // reset and clear text
          status_text.style.opacity = "1"
          status_text.innerHTML = ""
        }
      }
    }

  } else {
    console.log(`couldnt find status_text! ${status_text}`)
  }
}
