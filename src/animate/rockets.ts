import { getJiraKey } from "../jira";

const ROCKET_WIDTH = 80;

export function create_jira_rocket(jiraUrl: string, id: number, job: job) {
  const y = Math.random() * (window.innerWidth - ROCKET_WIDTH)

  const ticket = getJiraKey(job.ref, "")

  var div = document.createElement("div");
  div.id = "happyrocket" + id;
  div.style.position = "absolute";
  div.style.marginLeft = y + "px";
  div.style.top = window.innerHeight + "px"
  div.addEventListener("mouseenter", function(event: any) {
    if (event && event.target) {
      sessionStorage.setItem(div.id, "stopped");
      console.log(`stopping rocket ${div.id}!`)

      // put rocket ontop to avoid other rockets slipping in between current rocket and mouse
      var rocket = document.getElementById(div.id)
      rocket!.style.zIndex = "10"

      // display description
      var text = document.createElement("div")
      text.id = id + "text"
      div.style.position = "absolute";
      text.style.marginLeft = y + ROCKET_WIDTH + 5 + "px"
      text.style.top = document.getElementById(div.id)!.style.top
      text.style.opacity = "1"
      text.className = "rocket_description"
      var description_text = ticket ? `<a target="_blank" href="${jiraUrl}${ticket}">Ticket: ${ticket}</a><br>` : ""
      description_text += `Repo: <a target="_blank" href="${job.repo_url}">${job.repo_name}</a><br>`
      description_text += `Job: <a target="_blank" href="${job.web_url}">${job.name}</a><br>`
      description_text += `Stage: ${job.stage}<br>status: ${job.status}<br>`
      text.innerHTML = description_text

      document.body.appendChild(text)
    }
  }, false);
  div.addEventListener("mouseleave", function(event: any) {
    if (event && event.target) {
      // put rocket to back again
      var rocket = document.getElementById(div.id)
      rocket!.style.zIndex = "1"

      sessionStorage.removeItem(div.id);
      console.log(`starting rocket ${div.id}!`)
      var rocketText = document.getElementById(id + "text")!;

      // wait a second and then fade it out
      setTimeout(() => {
        function fade() {
          var opacity = parseFloat(rocketText.style.opacity)
          if (opacity > 0) {
            rocketText.style.opacity = (opacity - 0.03).toString()
            setTimeout(fade, 5)
          } else {
            rocketText!.remove()
          }
        }
        fade()
      }, 1300)
    }
  }, false);


  var img = document.createElement("img");
  img.className = "rocket_image"
  img.src = "rocket.png";
  img.id = "happyrocketimg" + id;

  var usrPic = document.createElement("img");
  usrPic.className = "rocket_user"
  usrPic.src = job.user_pic_url;
  usrPic.id = "happyrocketuserimg" + id;

  var text = document.createElement("div")
  text.className = "rocket_text"
  text.innerHTML = `<a target="_blank" href="${jiraUrl}${ticket}">${ticket}</a>`

  div.appendChild(img)
  div.appendChild(usrPic)
  div.appendChild(text)
  document.body.appendChild(div)
  return div.id
}

export function create_rocket(y = 0, id: number) {
  var img = document.createElement("img");
  img.id = "happyrocket" + id;
  img.src = "rocket.png";
  img.className = "rocket_image";
  img.style.left = y + "px";
  img.style.top = window.innerHeight - 100 + "px"

  document.body.appendChild(img);
  return img.id
}

export function create_planet(name: string, x: number, y: number, imgSrc: string) {
  var div = document.createElement("div");
  div.id = "planet" + name;
  div.style.position = "absolute";
  div.style.left = x + "px";
  div.style.top = y + "px"

  var img = document.createElement("img");
  img.id = "planetimg" + name;
  img.src = imgSrc;
  img.className = "planet";
  img.innerText = name

  div.appendChild(img)
  document.body.appendChild(div);
  return div.id
}

// TODO add functionality that rocket stops when hovering over it
export function rocket_fly(id: string, delay: number, job: job) {
  var velocity = 1 //Math.round(Math.random() * 100) / 100
  var rocket = document.getElementById(id);
  const will_explode = job.status == "failed" ? true : false
  // select an random explosion height within a radius
  const tenPercent = window.innerHeight / 10
  const explosionBaseline = (window.innerHeight / 2 - 3 * tenPercent)
  const explosion_height = explosionBaseline + (tenPercent - Math.random() * 2 * tenPercent)

  setTimeout(() => { console.log("woosshhhh"); loop() }, delay * 1000)

  async function loop() {
    if (rocket) {
      var top = parseInt(rocket.style.top.replace("px", ""))
      if (top <= -300) {
        rocket.remove()
        return
      }

      if (will_explode && top <= explosion_height) {
        setTimeout(() => rocket!.remove(), 200)
        draw_explosion(id, parseInt(rocket.style.marginLeft.replace("px", "")), top)
        return
      }

      if (sessionStorage.getItem(id) == "stopped") {
        console.log("not moving rocket since it is paused!")
      } else {
        top = top - velocity;
        rocket.style.top = top + "px";
      }
      setTimeout(loop, 10);
    }
  }
}

export function draw_explosion(id: string, x: number, y: number) {
  var img = document.createElement("img");
  img.style.marginLeft = x + "px";
  img.style.top = y + "px"
  img.id = "explosion" + id;
  img.src = "explosion.gif";
  img.className = "rocket_explosion";

  document.body.appendChild(img)

  setTimeout(() => { img.remove() }, 980)
}
