import { create_jira_rocket, rocket_fly } from "./animate/rockets";
import { updateStatus } from "./animate/text";
import { getAllDeployments, loadProjects } from "./gitlab";

async function main(): Promise<void> {
  const config = require('./../config.json')
  const mock = config.MOCK

  updateStatus("Dashboard started!", false, true)

  // get projects
  const projects = mock ? [] : await loadProjects()

  // load dummy data
  const dummyData = require('./dummy_data.json') as job[]

  var i = 0;
  refreshData()
  async function refreshData() {
    var currentDeployments = mock ? dummyData : await getAllDeployments(projects, config.GITLAB_TOKEN)

    currentDeployments.forEach((job: job) => {
      const rocket_id = create_jira_rocket(config.JIRA_URL, i, job)
      rocket_fly(rocket_id, job.seconds, job)
      i += 1
    });

    setTimeout(refreshData, 1000 * 60);
  }
}

main();
