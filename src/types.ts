type projectConfig = string[]

type job = {
  created_at: Date,
  author: string
  seconds: number
  ref: string,
  stage: string
  name: string
  user_pic_url: string
  web_url: string
  status: string
  repo_name: string
  repo_url: string
}
