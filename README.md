# Gitlab Rockets

Visualizes gitlab pipeline runs as tiny little rockets.

![gitlab rockets](.assets/animation.gif)

## Features

to be added soon:
- dummy data generation for better gif captures / testing


to be added at some point:
- a search bar to limit the scope
- add a rocket trail animation
- add special animation for production deployments
- "planet update"
  - have rockets orbit planets as long as the pipeline is still going on
  - have rockets crash into the planet if they succeed
  - add planets for the different environments
- add a spacy background animation


## Development

1. Put your gitlab API key in config.json
2. Start:
```bash
npm run start
```
3. Browse [index.html](index.html)
