import { getJiraKey } from "../src/jira"


describe("Jira key parse test", () => {
  test("given a jira key it should return the jira key", () => {
    const it = getJiraKey("ABC-22")

    expect(it).toEqual("ABC-22")
  })

  test("given a jira key surrounded by text it should return the jira key", () => {
    const it = getJiraKey("asd dd ABC-22 svadds")

    expect(it).toEqual("ABC-22")
  })

  test("given a jira key surrounded by weird it should return the jira key", () => {
    const it = getJiraKey("asd/ABC-22__/heheh")

    expect(it).toEqual("ABC-22")
  })

  test("Given not a jira key it should return a dummy value", () => {
    const it = getJiraKey("ABC- 22")

    expect(it).toEqual("?")
  })
})
