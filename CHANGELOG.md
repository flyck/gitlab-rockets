## [1.1.0](https://gitlab.com/flyck/gitlab-rockets/compare/v1.0.0...v1.1.0) (2022-04-10)


### Features

* improve jira key in description ([dd1f3de](https://gitlab.com/flyck/gitlab-rockets/commit/dd1f3deeb9e5a7f5edb31c65c5154369b57c6e29))

## 1.0.0 (2022-04-10)


### Features

* add dark mode ([77888f2](https://gitlab.com/flyck/gitlab-rockets/commit/77888f22c243ed260676a6047b5cfc8dc019bc6d))
* add explosion for failed jobs ([4827866](https://gitlab.com/flyck/gitlab-rockets/commit/4827866461fccebc36ddf5c2868cc63dbb2827ef))
* add fade-away for rocket description ([dcdc781](https://gitlab.com/flyck/gitlab-rockets/commit/dcdc781c838242e07fcfbcdf5a415f2ef7331899))
* add project list to session storage ([6dbd582](https://gitlab.com/flyck/gitlab-rockets/commit/6dbd582fd620e408ca3587ad8bb11dc73f610ee6))
* add rocket description, have rocket stop and display text ([2e3ff91](https://gitlab.com/flyck/gitlab-rockets/commit/2e3ff919fcf889860ae3f577bdf51d91a156be78))
* add rounded corners to rocket description ([b5a5482](https://gitlab.com/flyck/gitlab-rockets/commit/b5a5482831f6011bf1249ffe898dda3240ed16b8))
* improve rocket description ([c8db38e](https://gitlab.com/flyck/gitlab-rockets/commit/c8db38e36648aa41b43cc7b1d3a03e501bcb3533))
* improve rocket description ([d2987ac](https://gitlab.com/flyck/gitlab-rockets/commit/d2987acdf0227d7a910f26325a6468895240ec3b))
* improve status message in description ([c844c5b](https://gitlab.com/flyck/gitlab-rockets/commit/c844c5b15f359558c5c1851e99ff69070cf1ab04))
* reduce description text fadeaway time ([4b8c114](https://gitlab.com/flyck/gitlab-rockets/commit/4b8c114f4c291209f5583448206b7e6e5b2e7898))


### Bug Fixes

* add explosion gif(!) ([a58f112](https://gitlab.com/flyck/gitlab-rockets/commit/a58f11243c63a5e52da386bd0bc40f24226c9bb4))
* have rocket coordinates respect window, not screen ([e5fd777](https://gitlab.com/flyck/gitlab-rockets/commit/e5fd777eba6c933aca3eaf65062775796ed8a3e2))
* improve startup message ([dfa658b](https://gitlab.com/flyck/gitlab-rockets/commit/dfa658bfea510977e71789ace579cba56c1b2c4d))
* prevent rockets from flying over rocket descriptions ([879d3be](https://gitlab.com/flyck/gitlab-rockets/commit/879d3be011846431044e65b53b686f42e74a1c60))
* remove a display bug with rocket descriptions ([d1b5ebc](https://gitlab.com/flyck/gitlab-rockets/commit/d1b5ebc0aed5cde6d95e2654e1eb8c1ad6d69133))
* remove display bug by putting stopped rocket on top ([bf04d61](https://gitlab.com/flyck/gitlab-rockets/commit/bf04d61e03c48e58864ca34d75facb8315fe8642))
* skip jobs which didnt run properly ([59be426](https://gitlab.com/flyck/gitlab-rockets/commit/59be4269675d6f407af1b94b94a078d58c24ef38))
